fs      = require 'fs'
cp      = require 'child_process'
backup  = require 'mongobackup'
gulp    = require 'gulp'
watch   = require 'gulp-watch'
exist   = require 'file-exists'
$       = require('gulp-load-plugins')()


# Bip on error
onError = (err) ->
  $.util.beep()
  console.log err
  @emit 'end'
  return


env =
  sourcemap: true
  environment: process.env.NODE_ENV
  debug: false

console.log('Node environment is setup to: ',process.env.NODE_ENV)


# Create variables for our project paths so we can change in one place
paths =
  db:               '/data/db/'
  dbSave:           '/data/save'
  dbHost:           'localhost:27017'
  dbName:           'ahoy'
  server:           '../../server/'
  start:            'server/keystone.js'

  dbSaveServer:     '/var/db/save/'
  dbServer:         '/var/db'

  coffee:           'dev/**/*.coffee'
  coffeePublic:     'dev/public/coffee/**/*.coffee'
  coffeeServer:     'dev/server/coffee/**/*.coffee'
  jsPublic:         'public/js'
  jsServer:         'server'

  sass:             'dev/**/*{.scss,.sass}'
  sassPublic:       'dev/public/styles/'
  sassServer:       'dev/server/styles/'
  cssPublic:        'public/styles/'
  cssServer:        'node_modules/keystone/public/styles'

  image:            'public/images'

  hmtl:             'public/'


# Todo: add test methode to see if folders are created or not:
# check if server folder exist because it's not in git anymore
fs.mkdirSync('server') unless fs.existsSync('server')
#fs.mkdirSync(paths.dbSave) unless fs.existsSync(paths.dbSave)

# Compilations:
#-------------
gulp.task 'style', ->
  # public files
  gulp.src paths.sassPublic+'*{.scss,.sass}'
      .pipe $.plumber errorHandler: onError
      .pipe $.compass
        css:   paths.cssPublic
        image: paths.images
        sass:  paths.sassPublic
#        sourcemap: env.sourcemap
        debug: env.debug
      .pipe $.autoprefixer browsers: ['last 2 versions','ie >= 9']
      .pipe gulp.dest paths.cssPublic

  # server files
  gulp.src paths.sassServer+'keystone{.scss,.sass}'
      .pipe $.plumber errorHandler: onError
      .pipe $.compass
        css:   paths.cssServer
        sass:  paths.sassServer
      .pipe $.concat 'addon.less'
      .pipe gulp.dest paths.cssServer
  return


gulp.task 'coffee', ->
  conf =
    bare: true
    flatten: true

  # public files
  gulp.src paths.coffeePublic
      .pipe $.plumber errorHandler: onError
      .pipe $.coffee  conf
      .pipe $.concat 'scripts.js'
      .pipe gulp.dest paths.jsPublic

  # server files
  gulp.src paths.coffeeServer
      .pipe $.plumber errorHandler: onError
      .pipe $.coffee  conf
      .pipe gulp.dest paths.jsServer
  return


gulp.task "minify", ->
  gulp.src paths.html+'*.html'
      .pipe usemin
        cssmin:  $.minifyCss()
        htmlmin: $.minifyHtml()
        jsmin:   $.uglify()
      .pipe gulp.dest paths.html
  return

# Install script
installDependencies = ->
  cp.exec 'bower install', (err,stdeout,stderr) ->
    console.log(stdeout)
  cp.exec 'npm install', (err,stdeout,stderr) ->
    console.log(stdeout, stderr)
    addCustomAdminCSS()
  return

addCustomAdminCSS = ->
  cp.exec 'grep "addon.less" '+paths.cssServer+'/keystone.less', (err,stdeout,stderr) ->
    console.log(stdeout, stderr)
    unless stdeout
      console.log($.util.colors.green 'Add admin css to keystone')
      fs.appendFile paths.cssServer+'/keystone.less', '@import "addon.less";', (err) ->
        console.log(err)
        gulp.start('style')
  return

gulp.task 'install', ->
  unless exist paths.start
    console.log('Compile coffeescript')
    gulp.start('coffee')
  console.log($.util.colors.green 'Install dependencies')
  installDependencies()


  return

# Database operations:
#---------------------
gulp.task "dbstart", ->
  cp.exec 'pgrep mongo', (err,stdeout,stderr) ->
    # Check if database already started
    if stdeout.length
      console.log("Database ready, keep going...")
    else
      # Start database
      cp.exec 'mongod', (err,stdeout,stderr) ->
        console.log(stdeout)
  return

gulp.task 'dbdump', ->
  backup.dump
    host: paths.dbHost
    out: paths.dbSave
    db: paths.dbName
  return

gulp.task 'dbrestoredev', ->
  backup.restore
    host: paths.dbHost
    drop: true
    path: paths.dbSave+"/"+paths.dbName
  return

gulp.task 'dbrestore', ->
  backup.restore
    host: paths.dbHost
    drop: true
    path: paths.dbSaveServer+"/"+paths.dbName
  return

gulp.task "dbdrop", ['dbdump'], ->
  cp.exec 'mongo '+paths.dbName+' --eval "db.dropDatabase();"'
#  process.kill(process.pid, 'SIGUSR2')

# Main tasks:
#------------
gulp.task 'default', ->
  require paths.server+'keystone.js'

# gulp watcher
gulp.task 'dev', ['dbstart'], ->
  if exist paths.start
    $.nodemon
      verbose: false
      script: paths.start
      ext: 'js'
    gulp.watch paths.coffee, ['coffee']
    gulp.watch paths.sass, ['style']
  else
    $.util.log $.util.colors.red('\n There is no keystone.js file. Did you use the gulp install command? \n')
  return


# Test & help:
#-------------
gulp.task 'test', ->
  # Todo add test framework
  fs.readFile 'node_modules/keystone/public/styles/keystone.less', 'utf8',(err,data) ->
    return console.log(err) if (err)
    err = []
    err.push $.util.colors.red 'Add addon.less in keystone module stylesheet' if data.indexOf('addon.less')<0
    err.push $.util.colors.green 'All good bro' unless err.length

    $.util.log error for error in err


#gulp.task 'deployFile', ->
#
#  gulp.src '/Users/Pierre/Sites/Ahoy-server/'
#  .pipe($.debug())
#  .pipe $.sftp
#    host: '52.64.7.140'
#    user: 'pierre'
#    key: '/Users/Pierre/Documents/ahoy/amazon/pierre-key-pair.pem'
#    remotePath: '/var/www/ahoy'

gulp.task 'deployDB', ->

  gulp.src paths.dbSave+'/'+paths.dbName+'/*'
  .pipe($.debug())
  .pipe $.sftp
    host: '52.64.7.140'
    user: 'pierre'
    key: '/Users/Pierre/Documents/ahoy/amazon/pierre-key-pair.pem'
    remotePath: '/var/db/save/'+paths.dbName

help = ->
  c = $.util.colors.cyan
  $.util.log 'Gulp tasks:',
    c('\ngulp install:  '), 'Install npm and bower dependencies then add custum admin css and compile',
    c('\ngulp:          '), 'Start server',
    c('\ngulp style:    '), 'Compile sass to css',
    c('\ngulp coffee:   '), 'Compile coffeescript to js',
    c('\ngulp minify:   '), 'Minify html css and js',
    c('\ngulp dev:      '), 'Start server, watch dev folder for changes and compile',
    c('\ngulp test:     '), 'Test the dev environment',
    c('\ngulp dbstart:  '), 'Save and remove database (stop server and restart it)',
    c('\ngulp dbdump:   '), 'Start database (mongo)',
    c('\ngulp dbrestore:'), 'Stop database (mongo)'

  return
gulp.task 'help', help
