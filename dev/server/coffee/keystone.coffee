# Simulate config options from your production environment by
# customising the .env file in your project's root folder.
require('dotenv').load()

# Require keystone
keystone = require('keystone')

# Initialise Keystone with your project's configuration.
# See http://keystonejs.com/guide/config for available options
# and documentation.
keystone.init
  'name'         : 'ahoy'
  'brand'        : 'ahoy'
  'static'       : '../public'
  'favicon'      : '../public/favicon.ico'
  'views'        : '../dev/public/templates/views'
  'emails'       : '../dev/public/templates/emails'
  'view engine'  : 'jade'
  'user model'   : 'User'
  'cookie secret': '+^^afEzV+-RFNTOCc^_xg-5nMKf?8En*xizgN8;g~{CmO>%vt4`fK>+VcH2l4@TB'
  'auto update'  : true
  'session'      : true
  'auth'         : true


# Load your project's Models
keystone.import 'models'

# Setup common locals for the templates. The following are required for the
# bundled templates and layouts. Any runtime locals (that should be set uniquely
# for each request) should be added to ./routes/middleware.js
keystone.set 'locals',
  _: require('underscore')
  env: keystone.get('env')
  utils: keystone.utils
  editable: keystone.content.editable


# Load your project's Routes
keystone.set 'routes', require('../server/routes')

# Custom admin start
#importRoutes = keystone.importer(__dirname)
#middleware = require('./routes/middleware')
#routes =
#  views: importRoutes("routes")
#console.log(keystone.routes)
#keystone.app.get('/keystone/payment',middleware,require('./routes/views/payment'))
# Custom admin end

# Setup common locals for your emails. The following are required by Keystone's
# default email templates, you may remove them if you're using your own.

# Configure the navigation bar in Keystone's Admin UI
keystone.set 'nav',
  payment: [
    label: 'Payments'
    key: 'payment'
    path: '/payment'
#    external: true
  ]

  orders: [
    'orders'
  ]

  posts: [
    'posts'
    'post-categories'
  ]
  contacts: [
    'contacts'
  ]

  sms: [
    'sms-requests'
    'sms-responses'
  ]

  Admin: [
    'users'
  ,
    label: 'Home page'
    key : 'page-homes'
    path : '/admin/page-homes'
    external: true
  ,
    label: 'About page'
    key : 'page-abouts'
    path : '/admin/page-abouts'
    external: true
  ,
    'page-abouts'
  ,
    'page-homes'
  ,
    'page-news'
  ]


# Start Keystone to connect to your database and initialise the web server
keystone.start()
