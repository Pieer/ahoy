keystone = require("keystone")
Types = keystone.Field.Types

###*
Address Model
==========
###
Address = new keystone.List("Address")
Address.add
  isPrimary:
    type: Types.Boolean
    default: false

  label:
    type: String

  name:
    type: Types.Name

  addressLineOne:
    type: String

  addressLineTwo:
    type: String

  country:
    type: Types.Select
    options:[
      {
        value: "Australia"
        label: "Australia"
      }
    ]

  city:
    type: String

  state:
    type: Types.Select
    options: [
      value: "ACT"
      label: "ACT"
    ,
      value: "NSW"
      label: "NSW"
    ,
      value: "NT"
      label: "NT"
    ,
      value: "QLD"
      label: "QLD"
    ,
      value: "SA"
      label: "SA"
    ,
      value: "TAS"
      label: "TAS"
    ,
      value: "VIC"
      label: "VIC"
    ,
      value: "WA"
      label: "WA"
    ]

  postal:
    type: String

Address.register()

