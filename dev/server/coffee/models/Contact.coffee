keystone = require("keystone")
Types = keystone.Field.Types

###*
Contact Model
==========
###
Contact = new keystone.List("Contact",
  autokey:
    from: "phone"
    path: 'phone'
    fixed: true
    unique: true
#  drilldown: 'orders'
)
Contact.add
  name:
    type: Types.Name

  phone:
    type: String
    initial: true

  fax:
    type: String

  email:
    type: Types.Email

  stripeToken:
    type: String

  stripeId:
    type: String

  addressLineOne:
    type: String

  addressLineTwo:
    type: String

  country:
    type: Types.Select
    options:[
      {
        value: "Australia"
        label: "Australia"
      }
    ]

  city:
    type: String

  state:
    type: Types.Select
    options: [
      value: "ACT"
      label: "ACT"
    ,
      value: "NSW"
      label: "NSW"
    ,
      value: "NT"
      label: "NT"
    ,
      value: "QLD"
      label: "QLD"
    ,
      value: "SA"
      label: "SA"
    ,
      value: "TAS"
      label: "TAS"
    ,
      value: "VIC"
      label: "VIC"
    ,
      value: "WA"
      label: "WA"
    ]

  postal:
    type: String


  orders:
    type: Types.Relationship
    ref: 'Order'
    noedit: true
    many: true
    autocreate: true
#    initial: true
#    filters:
#      contact: ':_id'
###*
Relationships
###
Contact.relationship
  ref: "Order"
  path: "orders"
  refPath: 'order'

Contact.defaultColumns = "phone,name"

Contact.register()

