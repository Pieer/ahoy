keystone = require("keystone")
Types = keystone.Field.Types

###*
Order Model
==========
###
Order = new keystone.List("Order",
  autokey:
    path: 'slug'
    from: 'title'
    unique: true
  map:
    name: 'title'
  defaultSort: '-createdAt'

#  drilldown: 'contact'

)
Order.add

  client:
    type: Types.Relationship
    ref: 'Contact'
    noedit: true

  title:
    type: String
    required: true
    index: true

  description:
    type: String

  isPaid:
    type: Types.Boolean
    default: false

  createdAt:
    type: Date
    default: Date.now

  paymentId:
    type: String

  paymentDate: Date

  amount:
    type: Number

  currency:
    type: String

  status:
    type: String

  balance_transaction:
    type: String

  refundsUrl:
    type: String


###*
Relationships
###
Order.relationship
  ref: "Contact"
  path: "contacts"
  refPath: 'contact'


###*
Registration
###
Order.defaultColumns = "title"
Order.register()
