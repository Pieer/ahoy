keystone = require("keystone")
Types = keystone.Field.Types

###*
Enquiry Model
=============
###
Enquiry = new keystone.List("Enquiry")
Enquiry.add

  client:
    type: Types.Relationship
    ref: "Contact"
    many: false

  enquiryType:
    type: Types.Select
    options: [
      {
        value: "message"
        label: "Just leaving a message"
      }
      {
        value: "question"
        label: "I've got a question"
      }
      {
        value: "other"
        label: "Something else..."
      }
    ]


  message:
    type: String
    required: true
    default: ''

  createdAt:
    type: Date
    default: Date.now

Enquiry.defaultSort = "-createdAt"
Enquiry.defaultColumns = "client, enquiryType, createdAt"
Enquiry.register()
