keystone = require("keystone")
Types = keystone.Field.Types

###*
PageHome Model
==========
###
PageHome = new keystone.List("PageHome",
  map:
    name: "title"

  autokey:
    path: "slug"
    from: "title"
    unique: true
)
PageHome.add

  active:
    type: Types.Boolean
    index: true

  title:
    type: Types.Text
    default: 'We help our clients communicate'

  image:
    type: Types.CloudinaryImage

  intro:
    type: Types.Html
    wysiwyg: true
    height: 150

PageHome.register()
