
keystone = require("keystone")
Types = keystone.Field.Types

###*
MediaRelease Model
==========
###
MediaRelease = new keystone.List("MediaRelease",
  map:
    name: "title"

  autokey:
    path: "slug"
    from: "title"
    unique: true
)
MediaRelease.add
  title:
    type: String
    required: true

  state:
    type: Types.Select
    options: "draft, published, archived"
    default: "draft"
    index: true

  publishedDate:
    type: Types.Date
    index: true
    dependsOn:
      state: "published"

  pdf:
    type: Types.LocalFile
    dest: 'public/uploads'
    prefix: '/uploads/'

  content:
    brief:
      type: Types.Html
      wysiwyg: true
      height: 150

    extended:
      type: Types.Html
      wysiwyg: true
      height: 300

MediaRelease.schema.virtual("content.full").get ->
  @content.extended or @content.brief

MediaRelease.defaultColumns = "title, state|20%, author|20%, publishedDate|20%"
MediaRelease.register()
