keystone = require("keystone")
Contact = keystone.list("Contact")
exports = module.exports = (req, res) ->
  view = new keystone.View(req, res)
  locals = res.locals

  # Set locals
  locals.section = "register"
#  locals.country = Contact.fields.country.ops
  locals.states = Contact.fields.state.ops
  locals.formData = req.body or {}
  locals.validationErrors = {}
  locals.contactSubmitted = false
  locals.alreadyRegister = false

  # On POST requests, add the Enquiry item to the database
  view.on "post",
    action: "register"
  , (next) ->
    token = req.body.stripeToken
    if !token? or token is ''
      locals.contactSubmitted = false
      locals.validationErrors.noToken = true
      new keystone.Email('registration-error').send
        subject: 'Ahoy registration error: no token'
        to: 'schweiger.pierre@gmail.com'
        fromName: 'Admin'
        fromEmail: 'mate@helloahoy.com'
        enquiry:
          name:
            full: req.body['name.first']+' '+req.body['name.last']
          email: req.body.email
          phone: req.body.phone
          enquiryType: 'registration error'
          message: 'There is no token for that user.'
      , null
      next()
    else
      newContact = new Contact.model()
      updater = newContact.getUpdateHandler(req)
      updater.process req.body,
        flashErrors: false
        fields: "name, phone, email, addressLineOne, city, postal, state, country, stripeToken"
        errorMessage: "There was a problem submitting your registration:"
      , (err) ->
        if err
          if err.code != 11000
            locals.validationErrors = err.errors
          else
            locals.alreadyRegister = true
        else
          locals.contactSubmitted = true
        next()
        return

    return

  view.render "register"
  return
