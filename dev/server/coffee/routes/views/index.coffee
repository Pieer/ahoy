keystone = require 'keystone'

exports = module.exports = (req, res) ->
  view = new keystone.View(req, res)
  locals = res.locals
  locals.data = {}

  # locals.section is used to set the currently selected
  # item in the header navigation.
  locals.section = "home"

  # Load page informations
  view.on "init", (next) ->
    keystone.list("PageHome")
    .model
    .findOne()
    .exec (err, results) ->
      locals.data.page = results
      next err
      return
    return

  view.render "index"
  return

