#twitter    = require '../../lib/twitter'
#Autolinker = require 'autolinker'
keystone = require("keystone")

getNews = (req , res, loadFullPage) ->
  view = new keystone.View(req, res)
  locals = res.locals
  # Init locals
  locals.section = "news"
  locals.filters = category: req.params.category
#  locals.moment = require('moment')
#  locals.autolinker = new Autolinker()
  locals.data =
    posts: []
    categories: []
    page: []
    mediaReleases: []
    sideBarReleases: true

  if loadFullPage
    # Load page info
    view.on "init", (next) ->
      keystone.list("PageNew")
      .model
      .findOne()
      .exec (err, results) ->
        locals.data.page = results
        next err
        return

      return

    # Load all categories
    view.on "init", (next) ->
      keystone.list("PostCategory")
      .model
      .find()
      .sort("name")
      .exec (err, results) ->
        locals.data.categories = results
        next(err)
        return
      return

    # Load all media release
    view.on "init", (next) ->
      keystone.list("MediaRelease")
      .model
      .find()
      .where("state", "published").sort("-publishedDate")
      .limit(5)
      .exec (err, results) ->
        locals.data.mediaReleases.results = results
        next(err)
        return
      return

#    view.on "init", (next) ->
#      twitter.get (err, tweets) =>
#        if !err
#          locals.data.tweets = tweets
#        next err
#        return

  # Load the current category filter
  view.on "init", (next) ->
    if req.params.category
      keystone.list("PostCategory")
      .model.findOne(key: locals.filters.category)
      .exec (err, result) ->
        locals.data.category = result
        next err
        return
    else
      next()
    return

  # Load the posts
  view.on "init", (next) ->
    q = keystone.list("Post").paginate(
      page: req.query.page or 1
      perPage: 10
      maxPages: 10
    ).where("state").in(["published","featured"]).sort("-publishedDate").populate("categories")

    if (locals.data.category)
      q.where('categories').in([locals.data.category])

    q.exec (err, results) ->
      locals.data.posts = results
      next err
      return

    return

  # Render the view
  viewTemplate = "news"
  unless loadFullPage
    viewTemplate = "../mixins/post-brief"
  view.render viewTemplate
  return

exports.page = (req, res) ->
  getNews(req, res, true)
  return

exports.list = (req, res) ->
  getNews(req, res, false)
  return
