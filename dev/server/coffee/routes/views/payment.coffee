keystone = require("keystone")

Contact  = keystone.list("Contact")
Order    = keystone.list("Order")

if process.env.NODE_ENV is 'production'
  stripe = require('stripe')('sk_live_8y1zEl8imhOyL42RtIOMrrTW')
else
  stripe = require('stripe')('sk_test_6YV6Wk2JXEYGCgubr0JoMISv')


makePayment = (charge, next, req, res)->
  order = new Order.model()
  order.title = charge.title
  order.description = charge.description

  stripe.charges.create
    amount: charge.amount
    currency: 'aud'
    customer: charge.customerId
    description: charge.description
  , (err, charge) ->
    console.log(err, charge)
    if (err)
      # The card has been declined
      req.flash('error', err.message)
      next()

    else
      order.client = charge.customerId
      order.isPaid = true
      order.paymentId = charge.id
      order.paymentDate = charge.created
      order.amount = charge.amount
      order.currency = charge.currency
      order.status = charge.status
      order.balance_transaction = charge.balance_transaction
      order.refundsUrl = charge.refunds.url
      order.save()
      req.flash('info', 'Payment success')
      res.redirect('/keystone/orders')

exports = module.exports = (req, res) ->
  view = new keystone.View(req, res)
  locals = res.locals
  keystone.initNav()


  # locals.section is used to set the currently selected
  # item in the header navigation.
  locals.section = "Payment"
  locals.nav = keystone.nav
  locals.data =
    page: []

#    title: 'Ahoy',
#    orphanedLists: keystone.getOrphanedLists()

  # Load page info
  view.on "init", (next) ->
    keystone.list("Contact")
    .model
    .find()
    .exec (err, results) ->
#      console.log(results)
      locals.data.contacts = results
      next err
      return
    return

  view.on "post",
    action: "pay"
  , (next) ->
#    newPayment = new Payment.model()
#    updater = newPayment.getUpdateHandler(req)


    stripeToken = req.body.stripeToken
    charge =
      customerId: req.body.stripeId
      amount: req.body.amount # amount in cents
      title: req.body.title
      description: req.body.description

    if charge.customerId
      makePayment(charge, next, req, res)
    else
      stripCustomer =
        source: stripeToken
        email:  req.body.email
        metadata:
          phone: req.body.phone
          name: req.body.name
          addressLineOne: req.body.addressLineOne
          city: req.body.city
          postal: req.body.postal
          state: req.body.state
          country: req.body.country
        description: req.body.title
      stripe.customers.create(stripCustomer)
      .then (customer) ->
        charge.customerId = customer.id
        Contact.model.findById(req.body.contactId).exec (err, contact) ->
          if(err)
            console.log('There was a problem retreving the customer')
            next()
          else
            contact.stripeId = customer.id
            contact.save()
        makePayment(charge, next, req, res)
        return


    return

  # Render the view
  view.render "payment"
  return
