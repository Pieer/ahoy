#twitter    = require '../../lib/twitter'
#Autolinker = require 'autolinker'
keystone = require("keystone")
async = require("async")
exports = module.exports = (req, res) ->
  view = new keystone.View(req, res)
  locals = res.locals

  # Set locals
  locals.section = "news"
  locals.filters = post: req.params.post
#  locals.moment = require('moment')
#  locals.autolinker = new Autolinker()
  locals.data =
    posts: []
    page: []
    sideBarPost: true

  # Load the current post
  view.on "init", (next) ->
    q = keystone.list("Post").model.findOne(
      slug: locals.filters.post
    ).populate("author categories")
    q.exec (err, result) ->
      locals.data.post = result
      next err
      return

    return

#  # Load tweets
#  view.on "init", (next) ->
#    twitter.get (err, tweets) =>
#      if !err
#        locals.data.tweets = tweets
#      next err
#      return

  # Load page info
  view.on "init", (next) ->
    keystone.list("PageNew")
    .model
    .findOne()
    .exec (err, results) ->
      locals.data.page = results
      next err
      return

    return

  # Load other posts
  view.on "init", (next) ->
    keystone.list("Post")
    .model
    .find()
    .where("state", "published").sort("-publishedDate").populate("categories")
    .limit(3)
    .exec (err, results) ->

      locals.data.posts.results = results
      next(err)
      return
    return


  # Render the view
  view.render "post"
  return
