keystone = require("keystone")
Enquiry = keystone.list("Enquiry")
exports = module.exports = (req, res) ->
  view = new keystone.View(req, res)
  locals = res.locals
  
  # Set locals
  locals.section = "contact"
  locals.enquiryTypes = Enquiry.fields.enquiryType.ops
  locals.formData = req.body or {}
  locals.validationErrors = {}
  locals.enquirySubmitted = false
  
  # On POST requests, add the Enquiry item to the database
  view.on "post",
    action: "contact"
  , (next) ->
    newEnquiry = new Enquiry.model()
    updater = newEnquiry.getUpdateHandler(req)
    updater.process req.body,
      flashErrors: true
      fields: "name, email, phone, enquiryType, message"
      errorMessage: "There was a problem submitting your enquiry:"
    , (err) ->
      if err
        locals.validationErrors = err.errors
      else
        locals.enquirySubmitted = true
      next()
      return

    return

  view.render "contact"
  return