keystone = require("keystone")

exports = module.exports = (req, res) ->
  view = new keystone.View(req, res)
  locals = res.locals

  # locals.section is used to set the currently selected
  # item in the header navigation.
  locals.section = "about"

  locals.data =
    page: []

  # Load page info
  view.on "init", (next) ->
    keystone.list("PageAbout")
            .model
            .findOne()
            .exec (err, results) ->
              locals.data.page = results
              next err
              return
    return


  # Render the view
  view.render "about"
  return
