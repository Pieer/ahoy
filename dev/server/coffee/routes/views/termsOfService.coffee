keystone = require("keystone")

exports = module.exports = (req, res) ->
  view = new keystone.View(req, res)

  # Render the view
  view.render "terms-of-service"
  return
