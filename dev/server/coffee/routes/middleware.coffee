###*
This file contains the common middleware used by your routes.

Extend or replace these functions as your application requires.

This structure is not enforced, and just a starting point. If
you have more middleware you may want to group it as separate
modules in your project's /lib directory.
###
_ = require("underscore")
querystring = require("querystring")
keystone = require("keystone")
errors = require("../lib/errors")

###*
Initialises the standard view locals

The included layout depends on the navLinks array to generate
the navigation in the header, you may wish to change this array
or replace it with your own templates / logic.
###
exports.initLocals = (req, res, next) ->
  locals = res.locals
  locals.navLinks = [
      label: "Home"
      key: "home"
      href: "/"
    ,
#      label: "About us"
#      key: "about"
#      href: "/about-us"
#    ,
#      label: "Contact Us"
#      key: "contact"
#      href: "/contact"
#    ,
      label: "Services"
      key: "services"
      href: "/services"
    ,
      label: "Register"
      key: "register"
      href: "/register"
  ]
  locals.user = req.user

  next()
  return


###*
Fetches and clears the flashMessages before a view is rendered
###
exports.flashMessages = (req, res, next) ->
  flashMessages =
    info: req.flash("info")
    success: req.flash("success")
    warning: req.flash("warning")
    error: req.flash("error")

  res.locals.messages = (if _.any(flashMessages, (msgs) ->
    msgs.length
  ) then flashMessages else false)
  next()
  return

###*
    Inits the error handler functions into `res`
###

exports.initErrorHandlers = errors.initErrorHandlers



###*
Prevents people from accessing protected pages when they're not signed in
###
exports.requireUser = (req, res, next) ->
  unless req.user
    req.flash "error", "Please sign in to access this page."
    res.redirect "/keystone/signin"
  else
    next()
  return

exports.siteInfos = (req, res, next) ->
  # Load generic site informations

  keystone.list("Contact")
  .model
  .findOne()
  .exec (err, results) ->
    res.locals.siteData = results
    next()
    return
  return
