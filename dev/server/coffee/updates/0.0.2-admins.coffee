FakeData = require('../lib/generateData')
fakeData = new FakeData

contact = [
  name: 'Pierre'
  addressLineOne: '3704/184 Forbes street'
  addressLineTwo: 'Sydney NSW 2010'
  phone: '04 01 344 904'
  fax: ''
  email: 'schweiger.pierre@gmail.com'
  map: 'https://www.google.com.au/maps'
  linkedin: 'http://www.linkedin.com/company/'
  twitter: 'http://twitter.com/'
  facebook: 'http://facebook.com/'
  googleMap: 'https://www.google.com.au/maps'
  credits: '© ahoy. All rights reserved.'
]

# Initialise pages
pages =
  home: [
    title: 'Live Better & Get More Done.'
    active: true
    intro: 'Your personal concierge, available on-demand, 24/7 by SMS text message.'
  ]

  about: [
    title: 'About Us'
    intro: 'Ahoy combines all your favourite on-demand services.'
  ]

exports = module.exports = (done) ->
  fakeData.generate('Contact', contact, done)
  fakeData.generate('PageHome', pages.home, done)
  fakeData.generate('PageAbout', pages.about, done)
  return
