keystone = require('keystone')
async    = require('async')
faker    = require('faker')

wrapHtml = (content,tag) ->
  '<'+tag+'>'+content+'</'+tag+'>'

arrayOfForeignKeys = (list, nb) ->
  foreignKeys = []
  nb++
  # Add a number of foreign keys
  while nb -= 1
    random = Math.floor Math.random() * list.length
    foreignKeys.push list[random]['_id']
  require('uniq')(foreignKeys)
  foreignKeys

getModel = (model, callback) ->
  List = keystone.list(model)
  # return Promise
  List.model.find().exec().then (result) ->
    callback null, result

module.exports = class GenerateData
  constructor: () ->
    @list = {}
    @listName = ''

  sentence: ->
    faker.lorem.sentence()

  clone: (obj) ->
    return obj  if obj is null or typeof (obj) isnt "object"
    temp = new obj.constructor()
    for key of obj
      temp[key] = @clone(obj[key])
    temp

  htmlIpsum: (num,tag) ->
    result = ''
    num +=1 # end at 1
    if tag is 'li'
      while num -= 1
        result += wrapHtml(faker.lorem.words(),tag)
      result = result.replace(/,/g,' ')
      result = wrapHtml(result,'ul')
    else
      while num -= 1
        result += wrapHtml(faker.lorem.sentences(),tag)
    result

  randomDate: ->
    start = new Date(2012, 0, 1)
    end = new Date()
    date = new Date(start.getTime()+Math.random()*(end.getTime()-start.getTime()))
    return date

  createElement: (model, done) =>
    newElement = new @list.model(model)
    newElement.save (err) =>
      if err
        console.error err
      else
        console.log "Added '" + model[Object.keys(model)[0]] + "' to the table " + @listName + '.'
      done err
      return
    return

  generate: (model, elements, done) =>
    @listName = model
    @list = keystone.list(model)
    async.forEach elements, @createElement, done
    return

#  generateAll: (models, done) =>
#    for model, i in models
#      callback = null
#      if(i is models.length)
#        callback = done
#      @generate model.model, model.elements, callback
#    return

  populateWithForeignKeys: (newModel,models, fields, collection, done) ->
    async.mapSeries models, getModel, (err,results) =>
      if(err)
        throw err
      else
        # Loop through every Post in the array of posts
        for data in collection
          # Loop through the post fields that need to be updated with a foreign key
          for field,i in fields
            # The number of foreign key to be added is temporary in the template
            nb = data[field]
            if nb?
              # Replace the number of record by an array of ids
              aFkeys = arrayOfForeignKeys(results[i], nb)
              # Check if accept multiple value or not
              isUnique = ! keystone.list(newModel).fields[fields[i]].many
              # Return an array of object or a simple object
              if isUnique or aFkeys.length is 1
                data[field] = aFkeys[0]
              else
                data[field] = aFkeys
            else
              console.log('The field #{field} is not part of the model #{models[i]}')

        @generate(newModel, collection, done)


