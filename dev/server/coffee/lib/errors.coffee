syntaxHighlight = (json) ->
  json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\n/g,'<br>')
  json = json.replace(/&gt;/g, '<span class=\'sup\'>></span>')
  json.replace /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, (match) ->
    cls = 'number'
    if /^"/.test(match)
      if /:$/.test(match)
        cls = 'key'
      else
        cls = 'string'
    else if /true|false/.test(match)
      cls = 'boolean'
    else if /null/.test(match)
      cls = 'null'
    '<span class="' + cls + '">' + match + '</span>'

exports.initErrorHandlers = (req, res, next) ->

  res.err = (err, title, message) ->

    res.status(500).render 'errors/500',
      errorTitle: title
      errorMsg: syntaxHighlight(message)
    return

  res.notfound = (title, message) ->
    res.status(404).render 'errors/404'
    return

  next()
  return
