$ ->

  $recipient = $('#recipient')
  $recipient.addClass('in')

  showMore = (e) ->
    $.get '/api'+$(e.target).attr('href'), ( data ) ->
      $(e.target).parents('.showMoreRow').slideUp('fast')
      $recipient.append data
    false

  getPost = ->
    $.get '/api'+$(@).val(), ( data ) ->
      $recipient.removeClass('in')
      setTimeout( ->
        $recipient.html data
        setTimeout( ->
          $recipient.addClass('in')
          $('.sod_select').removeClass('above')
        ,100)
      ,1000)
    return


  $("select").on 'change', getPost


  $(document).on 'click', '.showMore', showMore

  return
