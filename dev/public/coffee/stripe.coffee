$ ->
  tokenField = $('[name="stripeToken"]')
  init = ->
    handler = StripeCheckout.configure(
      key: window.stripeKey
      image: '/images/app-icon.png'
      token: (token) ->
        tokenField.val(token.id)
        if(token.id isnt '')
          $('#registration').submit()
        else
          $('.invalid-form-error-message')
            .html("You must correctly fill the fields of at least one of these two blocks!")
    )

    formValidaton = $('#registration').parsley()

    $('#stripePopup').on 'click', (e) ->
      if(formValidaton.isValid())
#        $('#phone').val( parseInt($('#phone').val()))
        # Open Checkout with further options
        handler.open
          name: 'Ahoy'
          description: 'Please fill you credit card detail'
          email: $('#email').val()
          panelLabel: 'Save'
        e.preventDefault()
        return

    return
  if StripeCheckout?
    init()

  return
