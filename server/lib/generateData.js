var GenerateData, arrayOfForeignKeys, async, faker, getModel, keystone, wrapHtml,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

keystone = require('keystone');

async = require('async');

faker = require('faker');

wrapHtml = function(content, tag) {
  return '<' + tag + '>' + content + '</' + tag + '>';
};

arrayOfForeignKeys = function(list, nb) {
  var foreignKeys, random;
  foreignKeys = [];
  nb++;
  while (nb -= 1) {
    random = Math.floor(Math.random() * list.length);
    foreignKeys.push(list[random]['_id']);
  }
  require('uniq')(foreignKeys);
  return foreignKeys;
};

getModel = function(model, callback) {
  var List;
  List = keystone.list(model);
  return List.model.find().exec().then(function(result) {
    return callback(null, result);
  });
};

module.exports = GenerateData = (function() {
  function GenerateData() {
    this.generate = bind(this.generate, this);
    this.createElement = bind(this.createElement, this);
    this.list = {};
    this.listName = '';
  }

  GenerateData.prototype.sentence = function() {
    return faker.lorem.sentence();
  };

  GenerateData.prototype.clone = function(obj) {
    var key, temp;
    if (obj === null || typeof obj !== "object") {
      return obj;
    }
    temp = new obj.constructor();
    for (key in obj) {
      temp[key] = this.clone(obj[key]);
    }
    return temp;
  };

  GenerateData.prototype.htmlIpsum = function(num, tag) {
    var result;
    result = '';
    num += 1;
    if (tag === 'li') {
      while (num -= 1) {
        result += wrapHtml(faker.lorem.words(), tag);
      }
      result = result.replace(/,/g, ' ');
      result = wrapHtml(result, 'ul');
    } else {
      while (num -= 1) {
        result += wrapHtml(faker.lorem.sentences(), tag);
      }
    }
    return result;
  };

  GenerateData.prototype.randomDate = function() {
    var date, end, start;
    start = new Date(2012, 0, 1);
    end = new Date();
    date = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    return date;
  };

  GenerateData.prototype.createElement = function(model, done) {
    var newElement;
    newElement = new this.list.model(model);
    newElement.save((function(_this) {
      return function(err) {
        if (err) {
          console.error(err);
        } else {
          console.log("Added '" + model[Object.keys(model)[0]] + "' to the table " + _this.listName + '.');
        }
        done(err);
      };
    })(this));
  };

  GenerateData.prototype.generate = function(model, elements, done) {
    this.listName = model;
    this.list = keystone.list(model);
    async.forEach(elements, this.createElement, done);
  };

  GenerateData.prototype.populateWithForeignKeys = function(newModel, models, fields, collection, done) {
    return async.mapSeries(models, getModel, (function(_this) {
      return function(err, results) {
        var aFkeys, data, field, i, isUnique, j, k, len, len1, nb;
        if (err) {
          throw err;
        } else {
          for (j = 0, len = collection.length; j < len; j++) {
            data = collection[j];
            for (i = k = 0, len1 = fields.length; k < len1; i = ++k) {
              field = fields[i];
              nb = data[field];
              if (nb != null) {
                aFkeys = arrayOfForeignKeys(results[i], nb);
                isUnique = !keystone.list(newModel).fields[fields[i]].many;
                if (isUnique || aFkeys.length === 1) {
                  data[field] = aFkeys[0];
                } else {
                  data[field] = aFkeys;
                }
              } else {
                console.log('The field #{field} is not part of the model #{models[i]}');
              }
            }
          }
          return _this.generate(newModel, collection, done);
        }
      };
    })(this));
  };

  return GenerateData;

})();
