var exports, keystone;

keystone = require("keystone");

exports = module.exports = function(req, res) {
  var locals, view;
  view = new keystone.View(req, res);
  locals = res.locals;
  locals.section = "services";
  locals.data = {
    page: []
  };
  view.render("services");
};
