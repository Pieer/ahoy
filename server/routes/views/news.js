var getNews, keystone;

keystone = require("keystone");

getNews = function(req, res, loadFullPage) {
  var locals, view, viewTemplate;
  view = new keystone.View(req, res);
  locals = res.locals;
  locals.section = "news";
  locals.filters = {
    category: req.params.category
  };
  locals.data = {
    posts: [],
    categories: [],
    page: [],
    mediaReleases: [],
    sideBarReleases: true
  };
  if (loadFullPage) {
    view.on("init", function(next) {
      keystone.list("PageNew").model.findOne().exec(function(err, results) {
        locals.data.page = results;
        next(err);
      });
    });
    view.on("init", function(next) {
      keystone.list("PostCategory").model.find().sort("name").exec(function(err, results) {
        locals.data.categories = results;
        next(err);
      });
    });
    view.on("init", function(next) {
      keystone.list("MediaRelease").model.find().where("state", "published").sort("-publishedDate").limit(5).exec(function(err, results) {
        locals.data.mediaReleases.results = results;
        next(err);
      });
    });
  }
  view.on("init", function(next) {
    if (req.params.category) {
      keystone.list("PostCategory").model.findOne({
        key: locals.filters.category
      }).exec(function(err, result) {
        locals.data.category = result;
        next(err);
      });
    } else {
      next();
    }
  });
  view.on("init", function(next) {
    var q;
    q = keystone.list("Post").paginate({
      page: req.query.page || 1,
      perPage: 10,
      maxPages: 10
    }).where("state")["in"](["published", "featured"]).sort("-publishedDate").populate("categories");
    if (locals.data.category) {
      q.where('categories')["in"]([locals.data.category]);
    }
    q.exec(function(err, results) {
      locals.data.posts = results;
      next(err);
    });
  });
  viewTemplate = "news";
  if (!loadFullPage) {
    viewTemplate = "../mixins/post-brief";
  }
  view.render(viewTemplate);
};

exports.page = function(req, res) {
  getNews(req, res, true);
};

exports.list = function(req, res) {
  getNews(req, res, false);
};
