var async, exports, keystone;

keystone = require("keystone");

async = require("async");

exports = module.exports = function(req, res) {
  var locals, view;
  view = new keystone.View(req, res);
  locals = res.locals;
  locals.section = "news";
  locals.filters = {
    post: req.params.post
  };
  locals.data = {
    posts: [],
    page: [],
    sideBarPost: true
  };
  view.on("init", function(next) {
    var q;
    q = keystone.list("Post").model.findOne({
      slug: locals.filters.post
    }).populate("author categories");
    q.exec(function(err, result) {
      locals.data.post = result;
      next(err);
    });
  });
  view.on("init", function(next) {
    keystone.list("PageNew").model.findOne().exec(function(err, results) {
      locals.data.page = results;
      next(err);
    });
  });
  view.on("init", function(next) {
    keystone.list("Post").model.find().where("state", "published").sort("-publishedDate").populate("categories").limit(3).exec(function(err, results) {
      locals.data.posts.results = results;
      next(err);
    });
  });
  view.render("post");
};
