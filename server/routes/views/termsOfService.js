var exports, keystone;

keystone = require("keystone");

exports = module.exports = function(req, res) {
  var view;
  view = new keystone.View(req, res);
  view.render("terms-of-service");
};
