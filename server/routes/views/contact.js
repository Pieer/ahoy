var Enquiry, exports, keystone;

keystone = require("keystone");

Enquiry = keystone.list("Enquiry");

exports = module.exports = function(req, res) {
  var locals, view;
  view = new keystone.View(req, res);
  locals = res.locals;
  locals.section = "contact";
  locals.enquiryTypes = Enquiry.fields.enquiryType.ops;
  locals.formData = req.body || {};
  locals.validationErrors = {};
  locals.enquirySubmitted = false;
  view.on("post", {
    action: "contact"
  }, function(next) {
    var newEnquiry, updater;
    newEnquiry = new Enquiry.model();
    updater = newEnquiry.getUpdateHandler(req);
    updater.process(req.body, {
      flashErrors: true,
      fields: "name, email, phone, enquiryType, message",
      errorMessage: "There was a problem submitting your enquiry:"
    }, function(err) {
      if (err) {
        locals.validationErrors = err.errors;
      } else {
        locals.enquirySubmitted = true;
      }
      next();
    });
  });
  view.render("contact");
};
