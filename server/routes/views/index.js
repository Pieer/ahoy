var exports, keystone;

keystone = require('keystone');

exports = module.exports = function(req, res) {
  var locals, view;
  view = new keystone.View(req, res);
  locals = res.locals;
  locals.data = {};
  locals.section = "home";
  view.on("init", function(next) {
    keystone.list("PageHome").model.findOne().exec(function(err, results) {
      locals.data.page = results;
      next(err);
    });
  });
  view.render("index");
};
