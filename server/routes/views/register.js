var Contact, exports, keystone;

keystone = require("keystone");

Contact = keystone.list("Contact");

exports = module.exports = function(req, res) {
  var locals, view;
  view = new keystone.View(req, res);
  locals = res.locals;
  locals.section = "register";
  locals.states = Contact.fields.state.ops;
  locals.formData = req.body || {};
  locals.validationErrors = {};
  locals.contactSubmitted = false;
  locals.alreadyRegister = false;
  view.on("post", {
    action: "register"
  }, function(next) {
    var newContact, token, updater;
    token = req.body.stripeToken;
    if ((token == null) || token === '') {
      locals.contactSubmitted = false;
      locals.validationErrors.noToken = true;
      new keystone.Email('registration-error').send({
        subject: 'Ahoy registration error: no token',
        to: 'schweiger.pierre@gmail.com',
        fromName: 'Admin',
        fromEmail: 'mate@helloahoy.com',
        enquiry: {
          name: {
            full: req.body['name.first'] + ' ' + req.body['name.last']
          },
          email: req.body.email,
          phone: req.body.phone,
          enquiryType: 'registration error',
          message: 'There is no token for that user.'
        }
      }, null);
      next();
    } else {
      newContact = new Contact.model();
      updater = newContact.getUpdateHandler(req);
      updater.process(req.body, {
        flashErrors: false,
        fields: "name, phone, email, addressLineOne, city, postal, state, country, stripeToken",
        errorMessage: "There was a problem submitting your registration:"
      }, function(err) {
        if (err) {
          if (err.code !== 11000) {
            locals.validationErrors = err.errors;
          } else {
            locals.alreadyRegister = true;
          }
        } else {
          locals.contactSubmitted = true;
        }
        next();
      });
    }
  });
  view.render("register");
};
