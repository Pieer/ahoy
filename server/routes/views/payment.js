var Contact, Order, exports, keystone, makePayment, stripe;

keystone = require("keystone");

Contact = keystone.list("Contact");

Order = keystone.list("Order");

if (process.env.NODE_ENV === 'production') {
  stripe = require('stripe')('sk_live_8y1zEl8imhOyL42RtIOMrrTW');
} else {
  stripe = require('stripe')('sk_test_6YV6Wk2JXEYGCgubr0JoMISv');
}

makePayment = function(charge, next, req, res) {
  var order;
  order = new Order.model();
  order.title = charge.title;
  order.description = charge.description;
  return stripe.charges.create({
    amount: charge.amount,
    currency: 'aud',
    customer: charge.customerId,
    description: charge.description
  }, function(err, charge) {
    console.log(err, charge);
    if (err) {
      req.flash('error', err.message);
      return next();
    } else {
      order.client = charge.customerId;
      order.isPaid = true;
      order.paymentId = charge.id;
      order.paymentDate = charge.created;
      order.amount = charge.amount;
      order.currency = charge.currency;
      order.status = charge.status;
      order.balance_transaction = charge.balance_transaction;
      order.refundsUrl = charge.refunds.url;
      order.save();
      req.flash('info', 'Payment success');
      return res.redirect('/keystone/orders');
    }
  });
};

exports = module.exports = function(req, res) {
  var locals, view;
  view = new keystone.View(req, res);
  locals = res.locals;
  keystone.initNav();
  locals.section = "Payment";
  locals.nav = keystone.nav;
  locals.data = {
    page: []
  };
  view.on("init", function(next) {
    keystone.list("Contact").model.find().exec(function(err, results) {
      locals.data.contacts = results;
      next(err);
    });
  });
  view.on("post", {
    action: "pay"
  }, function(next) {
    var charge, stripCustomer, stripeToken;
    stripeToken = req.body.stripeToken;
    charge = {
      customerId: req.body.stripeId,
      amount: req.body.amount,
      title: req.body.title,
      description: req.body.description
    };
    if (charge.customerId) {
      makePayment(charge, next, req, res);
    } else {
      stripCustomer = {
        source: stripeToken,
        email: req.body.email,
        metadata: {
          phone: req.body.phone,
          name: req.body.name,
          addressLineOne: req.body.addressLineOne,
          city: req.body.city,
          postal: req.body.postal,
          state: req.body.state,
          country: req.body.country
        },
        description: req.body.title
      };
      stripe.customers.create(stripCustomer).then(function(customer) {
        charge.customerId = customer.id;
        Contact.model.findById(req.body.contactId).exec(function(err, contact) {
          if (err) {
            console.log('There was a problem retreving the customer');
            return next();
          } else {
            contact.stripeId = customer.id;
            return contact.save();
          }
        });
        makePayment(charge, next, req, res);
      });
    }
  });
  view.render("payment");
};
