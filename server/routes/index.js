
/**
This file is where you define your application routes and controllers.

Start by including the middleware you want to run for every request;
you can attach middleware to the pre('routes') and pre('render') events.

For simplicity, the default setup for route controllers is for each to be
in its own file, and we import all the files in the /routes/views directory.

Each of these files is a route controller, and is responsible for all the
processing that needs to happen for the route (e.g. loading data, handling
form submissions, rendering the view template, etc).

Bind each route pattern your application should respond to in the function
that is exported from this module, following the examples below.

See the Express application routing documentation for more information:
http://expressjs.com/api.html#app.VERB
 */
var _, exports, getAdminPage, importRoutes, keystone, middleware, routes;

_ = require("underscore");

keystone = require("keystone");

middleware = require("./middleware");

importRoutes = keystone.importer(__dirname);

keystone.pre("routes", middleware.initLocals);

keystone.pre("render", middleware.flashMessages);

keystone.pre('routes', middleware.initErrorHandlers);

keystone.pre("render", middleware.siteInfos);

routes = {
  views: importRoutes("./views"),
  api: importRoutes('./api')
};

getAdminPage = function(req, res) {
  var param;
  param = req.params.page;
  return keystone.list(param).model.findOne({}, '_id', function(err, el) {
    if (el) {
      return res.redirect('/keystone/' + param + '/' + el._id);
    } else {
      req.flash('error', 'Item ' + req.params.item + ' could not be found.');
      return res.redirect('/keystone/' + param);
    }
  });
};

keystone.set('404', function(req, res, next) {
  res.notfound();
});

keystone.set('500', function(err, req, res, next) {
  var message, title;
  title = void 0;
  message = void 0;
  if (err instanceof Error) {
    message = err.message;
    err = err.stack;
  }
  res.err(err, title, message);
});

exports = module.exports = function(app) {
  app.get("/", routes.views.index);
  app.get("/api/sms-request", routes.api.smsRequest.post);
  app.get('/news?', routes.views.news.page);
  app.get('/news/post/:post', routes.views.post);
  app.get('/news/:category?', routes.views.news.page);
  app.get('/api/news?', routes.views.news.list);
  app.get('/api/news/:category', routes.views.news.list);
  app.get("/services", routes.views.services);
  app.all("/register", routes.views.register);
  app.get("/privacy-policy", routes.views.privacyPolicy);
  app.get("/terms-of-service", routes.views.termsOfService);
  app.get("/admin/:page", getAdminPage);
  app.all('/payment*', middleware.requireUser);
  app.all('/payment', routes.views.payment);
};
