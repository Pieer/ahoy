var async, keystone;

keystone = require("keystone");

async = require("async");


/*
 * JSON Version. Keep if we want to use angular
 */

exports.list = function(req, res) {
  var q;
  q = keystone.list("Post").paginate({
    page: req.query.page || 1,
    perPage: 7,
    maxPages: 10
  }).where("state", "published").sort("-publishedDate").populate("author categories");
  if (req.params.category) {
    q.where('categories')["in"]([req.params.category]);
  }
  q.exec(function(err, results) {
    if (err) {
      return res.apiError('database error', err);
    }
    res.apiResponse({
      posts: results
    });
  });
};
