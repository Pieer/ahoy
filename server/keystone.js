var keystone;

require('dotenv').load();

keystone = require('keystone');

keystone.init({
  'name': 'ahoy',
  'brand': 'ahoy',
  'static': '../public',
  'favicon': '../public/favicon.ico',
  'views': '../dev/public/templates/views',
  'emails': '../dev/public/templates/emails',
  'view engine': 'jade',
  'user model': 'User',
  'cookie secret': '+^^afEzV+-RFNTOCc^_xg-5nMKf?8En*xizgN8;g~{CmO>%vt4`fK>+VcH2l4@TB',
  'auto update': true,
  'session': true,
  'auth': true
});

keystone["import"]('models');

keystone.set('locals', {
  _: require('underscore'),
  env: keystone.get('env'),
  utils: keystone.utils,
  editable: keystone.content.editable
});

keystone.set('routes', require('../server/routes'));

keystone.set('nav', {
  payment: [
    {
      label: 'Payments',
      key: 'payment',
      path: '/payment'
    }
  ],
  orders: ['orders'],
  posts: ['posts', 'post-categories'],
  contacts: ['contacts'],
  sms: ['sms-requests', 'sms-responses'],
  Admin: [
    'users', {
      label: 'Home page',
      key: 'page-homes',
      path: '/admin/page-homes',
      external: true
    }, {
      label: 'About page',
      key: 'page-abouts',
      path: '/admin/page-abouts',
      external: true
    }, 'page-abouts', 'page-homes', 'page-news'
  ]
});

keystone.start();
