var SmsResponse, Types, keystone;

keystone = require("keystone");

Types = keystone.Field.Types;


/**
SmsResponse Model
=============
 */

SmsResponse = new keystone.List("SmsResponse");

SmsResponse.add({
  client: {
    type: Types.Relationship,
    ref: "Contact",
    many: false
  },
  responseType: {
    type: Types.Select,
    options: [
      {
        value: "question",
        label: "Request information"
      }, {
        value: "registration",
        label: "You are now register"
      }, {
        value: "process",
        label: "Your order is being process"
      }
    ]
  },
  message: {
    type: String,
    required: true,
    "default": ''
  },
  sender: {
    type: Types.Relationship,
    ref: "User",
    many: false
  },
  createdAt: {
    type: Date,
    "default": Date.now
  }
});

SmsResponse.defaultSort = "-createdAt";

SmsResponse.defaultColumns = "enquiryType, message, sender, createdAt";

SmsResponse.register();
