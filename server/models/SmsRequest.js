var SmsRequest, Types, keystone;

keystone = require("keystone");

Types = keystone.Field.Types;


/**
SmsRequest Model
==========
 */

SmsRequest = new keystone.List("SmsRequest", {
  map: {
    name: "title"
  }
});

SmsRequest.add({
  'message-id': {
    type: Types.Key,
    index: true
  },
  'account-id': {
    type: Types.Key
  },
  network: {
    type: String
  },
  from: {
    type: Types.Number,
    index: true
  },
  to: {
    type: Types.Number
  },
  body: {
    type: Types.Textarea
  },
  price: {
    type: Types.Money
  },
  'date-received': {
    type: Types.Date
  },
  'final-status': {
    type: String
  },
  'date-closed': {
    type: Types.Date
  },
  latency: {
    type: Types.Number
  },
  type: {
    type: String
  }
});

SmsRequest.defaultColumns = "from, date-received|20%, body|20%";

SmsRequest.register();
