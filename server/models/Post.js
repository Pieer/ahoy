var Post, Types, keystone;

keystone = require("keystone");

Types = keystone.Field.Types;


/**
Post Model
==========
 */

Post = new keystone.List("Post", {
  map: {
    name: "title"
  },
  autokey: {
    path: "slug",
    from: "title",
    unique: true
  },
  defaultSort: '-publishedDate'
});

Post.add({
  title: {
    type: String,
    required: true
  },
  metaKeywords: {
    type: Types.Text
  },
  metaDescription: {
    type: Types.Text
  },
  state: {
    label: 'Status',
    type: Types.Select,
    options: "draft, published, featured, archived",
    "default": "draft",
    index: true
  },
  author: {
    type: Types.Relationship,
    ref: "User",
    index: true
  },
  publishedDate: {
    type: Types.Date,
    index: true
  },
  image: {
    type: Types.CloudinaryImage
  },
  content: {
    brief: {
      type: Types.Html,
      wysiwyg: true,
      height: 150
    },
    extended: {
      type: Types.Html,
      wysiwyg: true,
      height: 600
    }
  },
  categories: {
    type: Types.Relationship,
    ref: "PostCategory",
    many: true
  },
  services: {
    type: Types.Relationship,
    ref: "Service",
    many: true
  }
});

Post.schema.virtual("content.full").get(function() {
  return this.content.extended || this.content.brief;
});

Post.defaultColumns = "title, state|20%, categories|20%, publishedDate|20%";

Post.register();
