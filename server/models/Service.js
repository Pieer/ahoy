var Service, Types, keystone;

keystone = require("keystone");

Types = keystone.Field.Types;


/**
Service Model
==========
 */

Service = new keystone.List("Service", {
  map: {
    name: "name"
  },
  sortable: true,
  autokey: {
    path: "slug",
    from: "name",
    unique: true
  }
});

Service.add({
  name: {
    type: String
  },
  smallImage: {
    type: Types.CloudinaryImage
  },
  image: {
    type: Types.CloudinaryImage
  },
  content: {
    brief: {
      type: Types.Html,
      wysiwyg: true,
      height: 150
    },
    extended: {
      type: Types.Html,
      wysiwyg: true,
      height: 600
    }
  },
  featuresList: {
    type: Types.Html,
    wysiwyg: true,
    height: 150
  }
});

Service.defaultColumns = "name, title";

Service.register();
