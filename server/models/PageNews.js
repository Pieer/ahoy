var PageNews, Types, keystone;

keystone = require("keystone");

Types = keystone.Field.Types;


/**
PageNews Model
==========
 */

PageNews = new keystone.List("PageNews", {
  map: {
    name: "title"
  },
  autokey: {
    path: "slug",
    from: "title",
    unique: true
  }
});

PageNews.add({
  title: {
    type: Types.Text,
    "default": 'We help our clients communicate'
  },
  image: {
    type: Types.CloudinaryImage
  },
  intro: {
    type: Types.Html,
    wysiwyg: true,
    height: 150
  }
});

PageNews.register();
