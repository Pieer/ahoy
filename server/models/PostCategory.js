var PostCategory, Types, keystone;

keystone = require("keystone");

Types = keystone.Field.Types;


/**
PostCategory Model
==================
 */

PostCategory = new keystone.List("PostCategory", {
  map: {
    name: "name"
  },
  autokey: {
    from: "name",
    path: "key",
    unique: true
  }
});

PostCategory.add({
  name: {
    type: String,
    required: true
  }
});

PostCategory.relationship({
  ref: "Post",
  path: "categories"
});

PostCategory.register();
