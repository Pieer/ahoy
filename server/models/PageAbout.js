var PageAbout, Types, keystone;

keystone = require("keystone");

Types = keystone.Field.Types;


/**
PageAbout Model
==========
 */

PageAbout = new keystone.List("PageAbout", {
  map: {
    name: "title"
  }
});

PageAbout.add({
  title: {
    type: Types.Text,
    "default": 'About Us'
  },
  image: {
    type: Types.CloudinaryImage,
    select: true
  },
  intro: {
    type: Types.Html,
    wysiwyg: true,
    height: 150
  }
});

PageAbout.defaultColumns = "title";

PageAbout.register();
