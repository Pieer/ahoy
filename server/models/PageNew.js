var PageNew, Types, keystone;

keystone = require("keystone");

Types = keystone.Field.Types;


/**
PageNew Model
==========
 */

PageNew = new keystone.List("PageNew", {
  autokey: {
    path: 'slug',
    from: 'name',
    unique: true
  },
  nodelete: true,
  nocreate: true,
  map: {
    name: 'title'
  }
});

PageNew.add({
  title: {
    type: Types.Text,
    "default": 'News & Insight'
  },
  metaKeywords: {
    type: Types.Text
  },
  metaDescription: {
    type: Types.Text
  },
  image: {
    type: Types.CloudinaryImage
  },
  columnTitle: {
    type: Types.Text,
    "default": 'Latest Media Releases'
  }
});

PageNew.defaultColumns = "title";

PageNew.register();
