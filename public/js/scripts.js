$(function() {
  var $recipient, getPost, showMore;
  $recipient = $('#recipient');
  $recipient.addClass('in');
  showMore = function(e) {
    $.get('/api' + $(e.target).attr('href'), function(data) {
      $(e.target).parents('.showMoreRow').slideUp('fast');
      return $recipient.append(data);
    });
    return false;
  };
  getPost = function() {
    $.get('/api' + $(this).val(), function(data) {
      $recipient.removeClass('in');
      return setTimeout(function() {
        $recipient.html(data);
        return setTimeout(function() {
          $recipient.addClass('in');
          return $('.sod_select').removeClass('above');
        }, 100);
      }, 1000);
    });
  };
  $("select").on('change', getPost);
  $(document).on('click', '.showMore', showMore);
});

$(function() {
  return $('.parallax').parallax();
});

$(function() {
  return $('select').not('.disabled').material_select();
});

$(function() {
  return $(".button-collapse").sideNav();
});

$(function() {
  var init, tokenField;
  tokenField = $('[name="stripeToken"]');
  init = function() {
    var formValidaton, handler;
    handler = StripeCheckout.configure({
      key: window.stripeKey,
      image: '/images/app-icon.png',
      token: function(token) {
        tokenField.val(token.id);
        if (token.id !== '') {
          return $('#registration').submit();
        } else {
          return $('.invalid-form-error-message').html("You must correctly fill the fields of at least one of these two blocks!");
        }
      }
    });
    formValidaton = $('#registration').parsley();
    $('#stripePopup').on('click', function(e) {
      if (formValidaton.isValid()) {
        handler.open({
          name: 'Ahoy',
          description: 'Please fill you credit card detail',
          email: $('#email').val(),
          panelLabel: 'Save'
        });
        e.preventDefault();
      }
    });
  };
  if (typeof StripeCheckout !== "undefined" && StripeCheckout !== null) {
    init();
  }
});
